
/**
 * 
 * @author dennis.franke
 * @version 1.0
 * @since 11.05.2022
 * 
 */

public class Person {
		
		private String name;
		private int alter;
		
		public Person() {
			
		}
		public Person(String name, int alter) {
			this.name = name;
			this.alter = alter;
		}
		public String getName() {
			return name;
		}
		
		/**
		 * 
		 *
		 * @param name
		 * @return void
		 * 
		 */
		
		public void setName(String name) {
			this.name = name;
		}
		public int getAlter() {
			return alter;
		}
		public void setAlter(int alter) {
			this.alter = alter;
		}
		
		
		public String toString() {
			return "Klasse: Person\n" + 
					"Name: " + this.name + "\n" + 
					"Alter: " + this.alter + "\n";
			
		}
	}


